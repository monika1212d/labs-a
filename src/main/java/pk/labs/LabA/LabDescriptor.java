package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "Display.class";
    public static String controlPanelImplClassName = "Controlpanel.class";

    public static String mainComponentSpecClassName = "Main.class";
    public static String mainComponentImplClassName = "Glowny.class";
    // endregion

    // region P2
    public static String mainComponentBeanName = null;
    public static String mainFrameBeanName = null;
    // endregion

    // region P3
    public static String sillyFrameBeanName = null;
    // endregion
}
