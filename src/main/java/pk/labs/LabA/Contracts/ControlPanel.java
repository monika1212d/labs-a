package pk.labs.LabA.Contracts;

import javax.swing.JPanel;


public interface ControlPanel {

    JPanel getPanel();

}